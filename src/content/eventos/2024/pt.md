---
title: Algorave Brasil 2024
pubDate: 2024-12-14
description: Algorave 2024
---

Este é o site da edição anual da Algorave Brasil, um evento Brasileiro organizado pela comunidade de mesmo nome, Algorave Brasil, para performances ao vivo de live coding no dia 14 de Dezembro de 2024 (sabado), de 9h a 00h, incluindo artistas visuais, artistas sonoros, músicos, não-músicos, programadoras, mestres em gambiologia, engenheiras, curiosos, iniciantes ou experientes.

O principal objetivo do evento é aproximar os interessados em [live coding](https://en.wikipedia.org/wiki/Live_coding) no
Brasil.

# Participe

Para participar basta incluir o seu nome e os detalhes da sua performance no link abaixo

[https://pad.riseup.net/p/AlgoraveBrasil2024-keep](https://pad.riseup.net/p/AlgoraveBrasil2024-keep)

# Programação

*Ainda sob definição*

| Hora  | Nome             | Cidade                                  | Descrição da atividade                                            |
|-------|------------------|-----------------------------------------|-------------------------------------------------------------------|
| 09:00 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 09:30 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 10:00 | igor medeiros    | sp                                      | supercollider                                                     |
| 10:30 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 11:00 | Cráudio          | Belo Horizonte                          | Som com Din, Guitarix, SooperLooper, Dumts (Pd)                   |
| 11:30 | berin            | Berlim                                  | Renardo fristaile                                                 |
| 12:00 | diegodukao       | Rio de Janeiro                          | Renardo freestyle                                                 |
| 12:30 | Pirarán          | (Mexico City, Morelia, MX, Hamilton,CA) | Performance híbrido: estuary+sintetizadores análogos y digitales) |
| 13:00 | Bruno Gola       | Berlim                                  | SuperCollider, NTMI, ffglitch                                     |
| 13:30 | fmIRAmar         | BH                                      | proLIXO (SuperCollider + XJADEO)                                  |
| 14:00 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 14:30 | Namdlogg         | Rio de Janeiro                          | experimentação orgânica                                           |
| 15:00 | taconi           | BH                                      | Brujeria Audiovisual (renardo + hydra)                            |
| 15:30 | Indizível        | Salvador/BA                             | Live patching com Pure Data                                       |
| 16:00 | batatassaura     | nunsei                                  | a descobrir                                                       |
| 16:30 | Maia Francisco   | Barcelona(ES)                           | Live code/SuperCollider, sound+visuals                            |
| 17:00 | desdansá         | São Paulo                               | livcode facet ou tidal                                            |
| 17:30 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 18:00 | BSBLOrk          | <CIDADE>                                | <DESCRICAO>                                                       |
| 18:30 | BSBLOrk          | <CIDADE>                                | <DESCRICAO>                                                       |
| 19:00 | Horário Livre    | <CIDADE>                                | <DESCRICAO>                                                       |
| 19:30 | Ángel Jara       | Buenos Aires                            | Música con Strudel REPL :-)                                       |
| 20:00 | Roda de Código   | São Paulo                               |                                                                   |
| 20:30 | Roda de Código   | São Paulo                               |                                                                   |
| 21:00 | Roda de Código   | São Paulo                               |                                                                   |
| 21:30 | Roda de Código   | São Paulo                               |                                                                   |
| 22:00 | Dunossauro       | Botucatu/SP                             | Sardine LoFi                                                      |
| 22:30 | lixt             | Pietrasanta-IT                          | threnoscope drones                                                |
| 23:00 | Bruxaria Digital | Natal/RN                                | Livecoding com Hydra                                              |
| 23:30 | Endemics         | Tkaronto/Toronto                        | Audiovisual (Hydra + Modular)                                     |


# Comunidades locais organizadas

| Cidade    | Local     | Slot           | Horário       | Organizadores |
|-----------|-----------|----------------|---------------|---------------|
| São Paulo | a definir | Roda de Código | 20:00 - 22:00 |               |


# Dúvidas?

Fale conosco através do nosso [grupo no telegram](tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA). Se você quer participar mas não sabe como, se precisa de uma mãozinha ou tem qualquer outra dúvida sobre o evento, fique à vontade para nos procurar pelo telegram, ele tem sido o meio "oficial" de comunicação com a comunidade Algorave Brasil

# Agradecimentos

A toda a comunidade Algorave Brasil.
