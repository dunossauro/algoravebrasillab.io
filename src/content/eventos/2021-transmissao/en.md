---
title: Streaming Instructions 2021
pubDate: 2021-12-11
description: Streaming Instructions 2021
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2020-transmissao/pt">português</a></li>
  <li><a href="/eventos/2020-transmissao/es">español</a></li>
  <li><a href="/eventos/2020-transmissao/en">english</a></li>
</ul>
<ul>
  <li>Each block has 30 minutes (maximum time), but you may reserve multiple blocks for your performance</li>
  <li>Each performance must start and end within its time frame</li>
  <li>Wait for the previous performance to finish - streaming early may interrupt it</li>
  <li>If possible, include information on the video, such as your name, location, etc</li>
  <li>We reccomend using Open Broadcaster Software (OBS), free software for Linux, Mac and Windows found at <a href="https://obsproject.com">https://obsproject.com</a>.</li>
  <li>You can meet the event's staff and artists before, during and after the event to take some beer &#127866; or questions in general at <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ul>

<h4 id="configuração">OBS Configuration</h4>
<p>Open OBS configuration inn "File" &gt; "Settings". There, access the following tabs to define the streaming parameters</p>

<ul>
  <li>On the “Stream” tab:
    <br />
    <img src="/obs-stream.png" />
    <ul>
      <li>Service = Custom ... </li>
      <li>Server = rtmp://biniou.net/algorave</li>
      <li>Stream Key = <em>COPY AND PASTE THE TOKEN YOU RECEIVED THROUGH EMAIL OR TELEGRAM</em></li>
    </ul>
  </li>
  <li>On the "Output" &gt; "Streaming" tab:
    <br />
    <img src="/obs-output.png" />
    <ul>
      <li>Video bitrate = 1000 Kbps
        <ul>
          <li>Tip!<em>1000 Kbps is a reasonable value which guarantees an average video quality. If throttling happens, try reducing it to 500 or lower. If the stream is stable and you requre more quality, try around 2000 - this requires a better and more reliable internet connection.</em></li>
        </ul>
      </li>
      <li>Audio bitrate = 128</li>
    </ul>
  </li>
  <li>On the "Video" tab:
    <br />
    <img src="/obs-video.png" />
    <ul>
      <li>Output Resolution = 1280x720.</li>
      <li>Common FPS values = 20
        <ul>
          <li>Tip!<em> If you experience throttling, try around 10 FPS. If you have good enough internet access, try 30 for a smoother stream</em></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>

<p>These configuration values define a medium quality but connection-wise safe stream. You may prefer to use higher bitrate, fps or resolution values if you have a more reliable internet connection - these are just suggestions.</p>

<h4 id="teste-sua-transmissão">Testing your Stream</h4>
<ol>
  <li>The tests should be done before the event begins.
    <ul>
      <li>The test stream <a href="https://youtu.be/lZxo0ftXxkA">Algorave Brasil 2020 (test stream)</a> will be available until 1:00 PM of 12/12 (saturday).</li>
      <li>It is possible to test after that using other channels (YouTube, Twitch.tv, etc), if you need help ask on <a href="tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA">Telegram</a> or in <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
    </ul>
  </li>
  <li>Begin streaming with "Start Streaming"
    <br />
    <img src="/obs-startstream.png" />
  </li>
  <li>Make sure you're live on "Algorave Brasil 2020 (test stream)" stream:
    <ul>
      <li><a href="https://youtu.be/lZxo0ftXxkA">https://youtu.be/lZxo0ftXxkA</a></li>
    </ul>
  </li>
</ol>

<p>If something goes wrong, this is a good reference to follow:</p>
<ul>
  <li><a href='https://support.google.com/youtube/answer/2853702?hl=en-GB'>'https://support.google.com/youtube/answer/2853702?hl=en-GB'</a></li>
</ul>

<h4 id="na-hora-da-performance">Streaming Live for Real</h4>
<ol>
  <li>Wait until your schedule, as listed on the <a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">event spreadsheet</a>.</li>
  <li>Begin streaming by pressing "Start Streaming"
  <br />
    <img src="/obs-startstream.png" />
  </li>
  <li>The stream will be live on the <a href='https://www.twitch.tv/algoravebrasil'>Algorave Brasil Twitch account</a> and also on <a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a>, aside from being available via RTMP at:
    <ul><li>rtmp://biniou.net/algorave/algoravebrasil</li></ul>
  </li>
  <li>Be ready to finish your stream a bit before your schedule ends.
    <br />
    <img src="/obs-stopstream.png" />
  </li>
  <li>That's it, we thank you for your participating and wait you on &#127866; <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ol>

<p><small>
  <h3>Credits:</h3>
  <ul>
    <li>
      The content on this page was inspired on the Eulerroom Equinox instructions, available at <a href="https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew">"https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew"</a>
    </li>
    <li>
      The RMTP server used for the Algorave Brasil 2020 stream was kindly provided by the free software project <a href="https://biniou.net">Le Biniou</a>.
    </li>
  </ul>
</small>
</p>
</div>
