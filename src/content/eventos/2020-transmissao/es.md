---
title: Instrucciones de transmisión 2020
pubDate: 2020-12-12
description: Instrucciones de transmisión 2020
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2020-transmissao/pt">português</a></li>
  <li><a href="/eventos/2020-transmissao/es">español</a></li>
  <li><a href="/eventos/2020-transmissao/en">english</a></li>
</ul>
<ul>
  <li>Los espacios son de 30 minutos (máximo), pero puedes tomar varios espacios si lo prefieres.</li>
  <li>Cada performance debe comenzar y terminar a tiempo.</li>
  <li>Inicia la transmisión a la hora correcta. Si ingresas antes puedes cortar la transmisión anterior.</li>
  <li>Si es posible, agrega información sobre el video, incluyendo tu nombre, ubicación, etc.</li>
  <li>Recomendamos utilizar Open Broadcaster Software (OBS), software gratuito disponible para Linux, Mac y Windows en <a href="https://obsproject.com">https://obsproject.com</a>.</li>
  <li>Usted puede encontrar la organización y artistas antes, durante y después del evento para tomar una cerveza &#127866; y preguntas generales en <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ul>

<h4 id="configuração">Configuración de OBS</h4>
<p>Abre la configuración de OBS en "File/Archivo" &gt; "Settings/Configuración". Allí, accede a las siguientes pestañas y configura los parámetros de transmisión:</p>

<ul>
  <li>Na aba “Stream/Transmisión”:
    <br />
    <img src="/obs-stream.png" />
    <ul>
      <li>Service/Servicio = Personalizado ... / Personalizado ...</li>
      <li>Server/Servidor = rtmp: //biniou.net/algorave</li>
      <li>Stream Key/Clave de transmisión = <em>COPIAR Y PEGAR TOKEN RECIBIDO POR CORREO ELECTRÓNICO O TELEGRAMA</em></li>
    </ul>
  </li>
  <li>En la pestaña "Output/Salida" &gt; "Streaming/Transmisión":
    <br />
    <img src="/obs-output.png" />
    <ul>
      <li>Video bitrate/Tasa de bits de video = 1000 Kbps
        <ul>
          <li>¡Ojo!<em>1000 Kbps de tasa de bits es un valor razonable y garantiza una calidad de video promedio, si observas cuellos de botella intenta reducirlos a 500, o si necesitas más valores de calidad de prueba alrededor de 2000, requiere una buena conexión a Internet pero garantiza una mejor calidad de video.</em></li>
        </ul>
      </li>
      <li>Audio bitrate/Tasa de bits de audio = 128</li>
    </ul>
  </li>
  <li>En la pestaña "Video":
    <br />
    <img src="/obs-video.png" />
    <ul>
      <li>Output (Scaled) resolution/Resolución de salida (escalada) = 1280x720.</li>
      <li>Common FPS values/Valores comunes de FPS = 20
        <ul>
          <li>¡Ojo! <em>Si ves cuellos de botella, prueba con 10 FPS, o si tienes una buena conexión a Internet, prueba con valores más altos como 30 FPS, por ejemplo.</em></li>
        </ul>
      </li>
    </ul>
  </li>
</ul>

<p>Estos valores de configuración definen una transmisión de calidad media y segura para evitar problemas de conexión, si tienes una buena conexión puedes usar valores de bitrate, fps o resolución más altos.Estas son solo sugerencias generales, es posible que prefieras definir sus propios parámetros.</p>

<h4 id="teste-sua-transmissão">Prueba tu transmisión (prueba de transmisión)</h4>
<ol>
  <li>Las pruebas deben realizarse antes de que comience el evento.
     <ul>
       <li>La transmisión de prueba <a href="https://youtu.be/lZxo0ftXxkA">Algorave Brasil 2020 (test stream)</a> solo estará disponible hasta las 13:00 (Brazilian time) del día 12 (sábado).</li>
       <li>Es posible hacer pruebas después de eso usando otros canales (YouTube, Twitch.tv, etc.), si necesita ayuda, pregunte en <a href="tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA">Telegram</a> o <a href="https://meet.jit.si/algoravebr">Backstage </a>.</li>
     </ul>
  </li>
  <li>Inicie la transmisión en "Start Streaming/Iniciar transmisión"
    <br />
    <img src="/obs-startstream.png" />
  </li>
  <li>Asegúrate de estar en vivo en el canal "Algorave Brasil 2020 (test stream)" en:
    <ul>
      <li><a href="https://youtu.be/lZxo0ftXxkA">https://youtu.be/lZxo0ftXxkA</a></li>
    </ul>
  </li>
  <li>Para finalizar la transmisión de prueba clic en "Stop streaming/Detener transmisión".
  <br />
    <img src="/obs-stopstream.png" />
  </li>
</ol>

<p>Una buena fuente de ayuda si algo sale mal con la prueba es esta:</p>
<ul>
  <li><a href='https://support.google.com/youtube/answer/2853702?hl=en-GB'>https://support.google.com/youtube/answer/2853702?hl=en-GB</a></li>
</ul>

<h4 id="na-hora-da-performance">En el momento de la performance (streaming oficial)</h4>
<ol>
  <li>Espera hasta la hora programada en la <a href="https://docs.google.com/spreadsheets/d/1TsNKt7erfQlH0SqALYQaoy9c1n_xuj30nMjEM-sqmzI/edit#gid=0">planilla del evento</a>.</li>
  <li>Comienza tu transmisión presionando el botón "Start Streaming/Comenzar a transmitir"
  <br />
    <img src="/obs-startstream.png" />
  </li>
  <li>La transmisión será en vivo en el canal<a href='https://www.twitch.tv/algoravebrasil'>Algorave Brasil en Twitch</a> y también en <a href="https://www.youtube.com/channel/UChqe--qhcErjPjCA9oJgq0w">YouTube</a>, además de estar también disponible vía RTMP en:
    <ul><li>rtmp://biniou.net/algorave/algoravebrasil</li></ul>
  </li>
  <li>Prepárate para finalizar la transmisión unos segundos antes de que finalice tu horario.
    <br />
    <img src="/obs-stopstream.png" />
  </li>
  <li>Eso es todo, agradecemos su participación y esperamos verlo en &#127866; <a href="https://meet.jit.si/algoravebr">Backstage</a>.</li>
</ol>

<p><small>
  <h3>Créditos:</h3>
  <ul>
    <li>
      El contenido de esta página se inspiró en las instrucciones del Equinox de Eulerroom, disponibles en <a href="https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew">https://docs.google.com/document/d/1qg5mpSqlyfCobvbvpVRzeiIgpdCZNUjp8nLxo0mGbdw/edit#heading=h.webm2cev83ew</a>
    </li>
    <li>
      El servidor RTMP utilizado para la transmisión de Algorave Brasil 2020 fue proporcionado amablemente por el proyecto de software libre <a href="https://biniou.net">Le Biniou</a>.
    </li>
  </ul>
</small>
</p>
</div>
