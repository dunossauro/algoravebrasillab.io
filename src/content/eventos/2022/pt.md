---
title: Algorave Brasil 2022 (pt)
pubDate: 2022-11-26
description: Algorave 2022
---

<div className="description">
<ul className="horizontal unstyled padded" style={{ maxWidth: "22rem", marginLeft: 0 }}>
  <li><a href="/eventos/2022/pt">português</a></li>
  <li>español</li>
  <li>english</li>
</ul>
<p>Este é o site da edição anual da Algorave Brasil, um evento
Brasileiro organizado pela comunidade de mesmo nome, Algorave Brasil,
para performances ao vivo de live coding no dia 26 de Novembro de 2022
(sabado), de 9h a 00h, incluindo artistas visuais, artistas sonoros,
músicos, não-músicos, programadoras, mestres em gambiologia,
engenheiras, curiosos, iniciantes ou experientes.</p>

<p>O principal objetivo do evento é aproximar os interessados em <a
href="https://en.wikipedia.org/wiki/Live_coding">live coding</a> no
Brasil.</p>

<iframe width="560" height="315" src="https://www.youtube.com/embed/S0_iRd8Uu1Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h1>Participe!</h1>
<p>(<em>inscrições encerradas</em>) <s>Para participar basta incluir o seu nome e os detalhes da sua performance no link abaixo.</s></p>

<ul>
  <li><a href="https://pad.riseup.net/p/AlgoraveBrasil2022-keep">https://pad.riseup.net/p/AlgoraveBrasil2022-keep</a></li>
</ul>

<h2>Instruções de transmissão (streaming)</h2>
<ul>
    <li><a href="/eventos/2022-transmissao/pt">Veja aqui as instruções de como transmitir a sua performance</a>.</li>
</ul>

<h1>Acompanhe ao vivo!</h1>
<ul>
  <li><a href="https://youtu.be/S0_iRd8Uu1Y">https://youtu.be/S0_iRd8Uu1Y</a></li>
</ul>

<h1 id="programacao">Programação</h1>

<table>
<tr><th>Hora</th><th>Nome</th><th>Cidade</th><th>Descrição da atividade</th></tr>
<tr><td colSpan="3"><em>Nota: Horário de Brasília (UTC -3)</em></td></tr>
<tr><td>10:00</td><td>z0rg</td><td>Paris</td><td>Some abstract geometric art with GLSL</td></tr>
<tr><td>10:30</td><td>Joenio e convidados</td><td>Paris</td><td>Papo de boas-vindas a Algorave Brasil 2022</td></tr>
<tr><td>11:00</td><td>Joenio</td><td>Paris</td><td>Performance, Hrung+TidalCycles+LeBiniou</td></tr>
<tr><td>11:30</td><td>Marimoura</td><td>Paris</td><td>Sonic PI</td></tr>
<tr><td>12:00</td><td>Bruno Gola + Berlin... </td><td></td><td></td></tr>
<tr><td>12:30</td><td>Bruno Gola + </td><td></td><td></td></tr>
<tr><td>13:00</td><td>berin</td><td>Berlin</td><td>performance</td></tr>
<tr><td>13:30</td><td>-free-</td><td></td><td></td></tr>
<tr><td>14:00</td><td>Igor Medeiros & Gil Fuser</td><td></td><td>SuperCollider JitLib</td></tr>
<tr><td>14:30</td><td>Igor Medeiros & Gil Fuser</td><td></td><td>SuperCollider JITLib</td></tr>
<tr><td>15:00</td><td>Indizível</td><td>Salvador</td><td>performance</td></tr>
<tr><td>15:30</td><td>namdlog</td><td>Rio de Janeiro</td><td>performance | strudel+tidalcycles</td></tr>
<tr><td>16:00</td><td>Ángel Jara</td><td>Buenos Aires</td><td>performance Streaming Online</td></tr>
<tr><td>16:30</td><td>batata</td><td>SP</td><td>performance</td></tr>
<tr><td>17:00</td><td>namdlog</td><td>Rio de Janeiro</td><td>performance | strudel+tidalcycles</td></tr>
<tr><td>17:30</td><td>porres</td><td>São paulo</td><td>performance</td></tr>
<tr><td>18:00</td><td>pablopablo</td><td>rio de janeiro</td><td>performance</td></tr>
<tr><td>18:30</td><td>diegodukao</td><td>Rio de Janeiro</td><td>performance</td></tr>
<tr><td>19:00</td><td>lowbin (Victor Hugo)</td><td>Brasília</td><td>Performance</td></tr>
<tr><td>19:30</td><td>azertype aka.r1</td><td>France</td><td>powered by Cooki collective & cyberflemme soft : sunvox/orca/ sonic pi</td></tr>
<tr><td>20:00</td><td>smosgasbord</td><td>Costa Rica</td><td>orca + supercollider</td></tr>
<tr><td>20:30</td><td>euFraktus_X</td><td>Brasilia</td><td>performance</td></tr>
<tr><td>21:00</td><td>BSBLOrk</td><td>Brasilia</td><td>performance</td></tr>
<tr><td>21:30</td><td>BSBLOrk</td><td>Brasilia</td><td>performance</td></tr>
<tr><td>22:00</td><td>Rafrobeat</td><td>La Calera, Colombia</td><td>tidalcycles + Hydra. Streaming online</td></tr>
<tr><td>22:30</td><td>afalfl</td><td>Paris France</td><td>FoxDot improv + orca algoambient notes, Streaming online</td></tr>
<tr><td>23:00</td><td>afalfl</td><td>Paris France</td><td>FoxDot improv + orca algoambient notes, Streaming online</td></tr>
<tr><td>23:30</td><td>Santiago Ramírez Camarena</td><td>Lima, Perú</td><td>Improvisación con FoxDot, MaxMsp y Arduino</td></tr>
</table>

<h1 id="comunidades">Comunidades locais organizadas</h1>
<ul>
  <li>Paris, FUZ hacker space https://fuz.re/#adresse, 1pm (Paris time), Joenio</li>
  <li>Berlin, -local nao definido-, -horario nao definido-, berin + Bruno Gola</li>
</ul>

<h1>Dúvidas?</h1>
<p>Fale conosco através do nosso <a href='tg://join?invite=BCotLVCAJTQ3fW5E-j9DVA'>grupo no telegram</a>. Se você quer participar mas não sabe como, se precisa de uma mãozinha ou tem qualquer outra dúvida sobre o evento, fique à vontade para nos procurar pelo telegram, ele tem sido o meio "oficial" de comunicação com a comunidade Algorave Brasil.</p>
<h1>Agradecimentos</h1>
<p>A toda a comunidade Algorave Brasil.</p>
</div>
