// Place any global data in this file.
// You can import this data from anywhere in your site by using the `import` keyword.

export const SITE_TITLE = "Algorave Brasil";
export const SITE_DESCRIPTION = "Bem-vindo!";
export const README_CADASTRO_ARTISTAS =
  "https://gitlab.com/algoravebrasil/algoravebrasil.gitlab.io/-/blob/master/README.md#querio-aparecer-na-listagem-de-artistas";
